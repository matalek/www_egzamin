
from django import forms
from models import *


class KomentarzForm(forms.ModelForm):
    class Meta:
        model = Komentarz
        exclude = ('pokemon', 'data')

    def __init__(self, *args, **kwargs):
        super(KomentarzForm, self).__init__(*args, **kwargs)
        # dodanie bootstrapowych klas
        for v in self.fields:
            self.fields[v].widget.attrs['class'] = 'form-control'

class AtakForm(forms.Form):
    atak = forms.ChoiceField()

