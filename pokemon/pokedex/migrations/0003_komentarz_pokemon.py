# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pokedex', '0002_komentarz'),
    ]

    operations = [
        migrations.AddField(
            model_name='komentarz',
            name='pokemon',
            field=models.ForeignKey(default=1, to='pokedex.Pokemon'),
            preserve_default=False,
        ),
    ]
