# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pokedex', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Komentarz',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pseudonim', models.CharField(max_length=32)),
                ('tresc', models.TextField(verbose_name=b'Tre\xc5\x9b\xc4\x87')),
            ],
        ),
    ]
