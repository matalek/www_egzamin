$(document).ready(function() {

    $("#oblicz").click(function(){
        console.log('kliknięto');

        var send_data = {};
        send_data['atak'] = $("#id_atak").val();
        send_data['numer'] = $("#numer").html();
        $.ajax({
            type: "POST",
            url: "/modyfikator/",
            data: send_data,
            success: function (data, textStatus, xhr) {
                $("#mnoznik").html('x' + data.mnoznik.toFixed(2));
                $("#blad").html("")
            },
            error: function(data) {
                $("#blad").html("Coś poszło nie tak!");
            }
        });
    });
});


// csrf for AJAX

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});