# -*- coding: utf-8 -*-

from django.db import models

class Pokemon(models.Model):
    numer = models.PositiveIntegerField(primary_key=True)
    nazwa = models.CharField(max_length=32, unique=True)
    ewolucja = models.ForeignKey('self', null=True, default=None)
    rodzaj1 = models.CharField(max_length=32)
    rodzaj2 = models.CharField(max_length=32, blank=True)

    def __str__(self):
        return ("#%s %s" % (self.numer, self.nazwa)).encode('utf-8', errors='replace')

class Skutecznosc(models.Model):
    atak = models.CharField(max_length=32)
    obrona = models.CharField(max_length=32)
    mnoznik = models.FloatField()
    
    class Meta:
        unique_together = ('atak', 'obrona')

    def __str__(self):
        return ("%s x %s" % (self.atak, self.obrona)).encode('utf-8', errors='replace')

class Komentarz(models.Model):
    pseudonim = models.CharField(max_length=32, blank=False)
    tresc = models.TextField(verbose_name='Treść', blank=False)
    pokemon = models.ForeignKey(Pokemon, null=False)
    data = models.DateTimeField(auto_now_add=True)