# -*- coding: utf-8 -*-

from django.shortcuts import render, get_object_or_404
from models import Pokemon
from forms import *
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.http import require_POST
import json
import datetime

def showList(request):
    pokemony = Pokemon.objects.all()
    return render(request, 'list.html', locals())

def pokaz(request, numer):
    p = get_object_or_404(Pokemon, pk=numer)
    
    if request.method == 'POST':
        form = KomentarzForm(request.POST)
        if form.is_valid():
            nowy_komentarz = form.save(commit=False)
            nowy_komentarz.pokemon = p
            nowy_komentarz.save()
            messages.add_message(request, messages.SUCCESS, 'Pomyślnie dodano komentarz!')
            return HttpResponseRedirect('/pokemon/' + numer + '/')
    else:
        form = KomentarzForm()

    komentarze = Komentarz.objects.filter(pokemon=p).order_by('-data')

    atak_form = AtakForm()
    ataki = Skutecznosc.objects.all().values('atak').distinct()
    ataki_list = []
    for a in ataki:
        ataki_list.append((a['atak'], a['atak']))
    atak_form.fields['atak'].widget.choices = ataki_list
    atak_form.fields['atak'].widget.attrs = {'class':'form-control'}

    return render(request, 'pokemon.html', locals())

@require_POST
def modyfikator(request):
    response_data = {}
    atak = request.POST.get('atak')
    numer = request.POST.get('numer')
    print numer
    try:
        pokemon = Pokemon.objects.get(numer=numer)
    except:
        return HttpResponse(
            status=400,
            content_type="application/json"
        )

    mnoznik = 1
    if pokemon.rodzaj1:
        try:
            s1 = Skutecznosc.objects.get(atak=atak, obrona=pokemon.rodzaj1)
            mnoznik *= s1.mnoznik
        except:
            ()

    if pokemon.rodzaj2:
        try:
            s1 = Skutecznosc.objects.get(atak=atak, obrona=pokemon.rodzaj2)
            mnoznik *= s1.mnoznik
        except:
            ()

    response_data['mnoznik'] = mnoznik
    return HttpResponse(
        json.dumps(response_data),
        content_type="application/json"
    )