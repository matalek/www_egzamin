Aleksander Matusiak, 347171
Egzamin WWW 2015

1. Dodałem bootstrapa, żeby się ładniej wyświetlało.

2. Dodałem panel admina dla modeli Pokemon i Skutczenosc. Dane logowania to admin/admin.

3. Dla każdego pokemona stworzyłem stronę, gdzie informacje wyświetlają sie w tabelce. Są też przy okazji odnośniki do ewolucji (przy czym sprawdzam, czy ewolucja jest w naszej bazie - dla wersji jeszcze bez Foreign Key).

4. Dodałem komentarze. Każdy komentarz ma też datę, żeby można było po niej sortować. Przy okazji wyświetlam ją. Żeby się ładnie wyświetlało, tak jak w Polsce sie powinno, zmieniłem LANGUAGE_CODE i TIME_ZONE w settings.py.

5. Dodałem ajaxowe wyliczanie mnożnika. Dane wyświetlane są z dokładnością 2 miejsc po przecinku. 

6. Dodałem wymagane migracje: są to migracje od 0004 do 0006. Migracja 0004 dodaje nową kolumnę dla klucza obcego (póki co z inną nazwą) i przypisuje w niej odpowiednie wartości na podstawie pola ewolucja. Migracja 0005 usuwa stare pole ewolucja, a 0006 zmienia nazwę nowego pola na właściwą.


