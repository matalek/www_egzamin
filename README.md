# „Web applications” exam #

### Purpose ###
The purpose of this task was to modify a simple database web application written in Django.

### My modifications ###

* I have added Bootstrap to enhance the design.
* I have added admin panel for models. Login: admin, password: admin.
* For each Pokemon I have created a page with informations displayed in a table. 
* I have enabled adding comments. Each comment has an appropriate date of creation. I have also changed the settings of the app, so as the date will be displayed in the format appropriate for Polish language.
* I have added calculating conversion rate using AJAX.
* I have changed a reference kept in string to a reference using foreign key. I have also added appropriate migrations for this, so as not to lose any data.

### Grade ###
In this exam I have achieved the highest possible grade (5).